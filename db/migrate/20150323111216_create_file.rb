class CreateFile < ActiveRecord::Migration
  def up
    create_table :uploads do |t|
      t.belongs_to :user, index: true
      t.string :title
      t.timestamps
    end
    add_attachment :uploads, :file

    create_table :tags do |t|
      t.string :title
    end
    create_table :tags_uploads, id: false do |t|
      t.belongs_to :upload, index: true
      t.belongs_to :tag, index: true
    end
  end
  def down
    drop_table :uploads
    drop_table :tags
    drop_table :tags_uploads
  end
end
