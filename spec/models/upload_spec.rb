require 'rails_helper'

RSpec.describe Upload, type: :model do
  it "not valid upload without file" do
    upload = Upload.new(title: "Some Title", tags: "tag1, tag2")
    expect(upload).not_to be_valid
  end

  it "not valid without title" do
    upload = Upload.new(tags: "tag1, tag2", :file => Rails.root.join("public/robots.txt").open)
    expect(upload).not_to be_valid
  end

  it "valid with uploaded file" do
    upload = Upload.new(title: "Some Title", tags: "tag1, tag2", :file => Rails.root.join("public/robots.txt").open)
    expect(upload).to be_valid
  end

  it "correct save with tags" do
    upload = Upload.create(title: "Some Title", tags: "tag1, tag2", :file => Rails.root.join("public/robots.txt").open)
    expect(upload.tags.pluck(:title)).to eq(["tag1", "tag2"])
  end

  it "file should have path" do
    upload = Upload.create!(title: "Some Title", tags: "tag1, tag2", :file => Rails.root.join("public/robots.txt").open)
    expect(Pathname.new(upload.file.path).open)
  end
end
