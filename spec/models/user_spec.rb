require 'rails_helper'

RSpec.describe User, :type => :model do
  it "users list" do
    test1 = User.create!(email: "test1@example.com", password: "password")
    test2 = User.create!(email: "test2@example.com", password: "password")

    expect(User.all).to eq([test1, test2])
  end

  it "not valid user without password" do
    test3 = User.new(email: "test3@example.com")
    expect(test3).not_to be_valid
  end
end
