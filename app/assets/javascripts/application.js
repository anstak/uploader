// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-fileupload
//= require bootstrap/dropdown
//= require bootstrap/alert
//= require bootstrap-tagsinput
//= require_tree .

$(function () {

    $('#fileupload').fileupload();

    var $form = $('#fileupload');

    $.getJSON($('#fileupload').prop('action'), function (files) {
      $form.fileupload('option', 'done').call($form, $.Event('done'), {result: {files: files}});
    });

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
      var inputs = data.context.find(':input');
      if (inputs.filter(function () {
              return !this.value && $(this).prop('required');
          }).first().focus().length) {
          data.context.find('button').prop('disabled', false);
          return false;
      }
      data.formData = inputs.serializeArray();
    });

    $('#fileupload').bind('fileuploadadd', function (e, data) {
      setTimeout(function() {
        $('.tagsinput').tagsinput()
      }, 50);
    });

});
