class Upload < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :tags

  has_attached_file :file

  validates_attachment_presence :file
  do_not_validate_attachment_file_type :file
  validates_presence_of :title

  include Rails.application.routes.url_helpers

  def to_jq_upload
    {
      "name" => read_attribute(:file_file_name),
      "size" => read_attribute(:file_file_size),
      "tags" => self.tags.pluck(:title).join(", "),
      "title" => read_attribute(:title),
      "url" => file.url(:original),
      "delete_url" => upload_path(self),
      "delete_type" => "DELETE"
    }
  end

  def tags=(tags_value)
    add_tags(tags_value)
  end

  def add_tags(string)
    string.split(",").map(&:strip).each do |tag|
      tags << Tag.find_or_create_by(title: tag)
    end
  end

end
